module Vulkan.Device where

import Import

import Control.Monad.Managed (Managed, managed)
import Control.Monad.Trans.Maybe (MaybeT(..), runMaybeT)
import Data.List (nub)
import Vulkan.Version (pattern MAKE_VERSION)

import qualified Data.ByteString as BS
import qualified RIO.Vector as Vector
import qualified SDL
import qualified SDL.Video.Vulkan as SDL (vkGetInstanceExtensions, vkCreateSurface)
import qualified Vulkan.Core10 as Vk
import qualified Vulkan.Core11 as Vk11
import qualified Vulkan.Extensions.VK_KHR_surface as Khr
import qualified Vulkan.Extensions.VK_KHR_swapchain as Khr
import qualified VulkanMemoryAllocator as VMA

withDevice :: SDL.Window -> Managed VulkanDevice
withDevice sdlWindow = do
  extensions <- liftIO do
    cstrings <- SDL.vkGetInstanceExtensions sdlWindow
    traverse BS.packCString cstrings

  -- TODO: debugExt setup
  let
    layers = map encodeUtf8
      []

    instanceCI = zero
      { Vk.enabledLayerNames =
          Vector.fromList layers
      , Vk.enabledExtensionNames =
          Vector.fromList extensions
      , Vk.applicationInfo = Just zero
          { Vk.applicationName = Just "playground 3"
          , Vk.apiVersion      = MAKE_VERSION 1 1 0
          }
      }
  vkInstance <- managed $ Vk.withInstance instanceCI Nothing bracket

  surface <- withSdlKhrSurface sdlWindow vkInstance
  physical <- getPhysicalDevice vkInstance surface
  withVulkanDevice vkInstance surface physical

withSdlKhrSurface :: SDL.Window -> Vk.Instance -> Managed Khr.SurfaceKHR
withSdlKhrSurface sdlWindow vkInstance = managed $ bracket create destroy
  where
    create =
      fmap Khr.SurfaceKHR $
        SDL.vkCreateSurface sdlWindow $
          castPtr (Vk.instanceHandle vkInstance)

    destroy o =
      Khr.destroySurfaceKHR vkInstance o Nothing

getPhysicalDevice :: MonadIO m => Vk.Instance -> Khr.SurfaceKHR -> m PhysicalDevice
getPhysicalDevice vkInstance surface = do
  (_result, devs) <- Vk.enumeratePhysicalDevices vkInstance
  scoreDevices (toList devs) >>= \case
    [] -> do
      SDL.showSimpleMessageBox Nothing SDL.Error "Vulkan setup error" "No matching devices."
      exitFailure
    (_score, best) : _rest ->
      pure best
  where
    scoreDevices devs =
      fmap
        (sortOn fst . catMaybes)
        (mapMaybeM scoreDevice devs)

    headM x = MaybeT case x of
      [] ->
        pure Nothing
      h : _ead ->
        pure (Just h)

    scoreDevice dev = runMaybeT do
      deviceHasSwapChain dev

      graphicsQueue <- headM =<< getGraphicsQueueIndices dev
      presentQueue  <- headM =<< getPresentQueueIndices dev
      presentMode   <- headM =<< getPresentMode dev
      bestFormat    <- getFormat dev

      props <- Vk.getPhysicalDeviceProperties dev
      caps  <- Khr.getPhysicalDeviceSurfaceCapabilitiesKHR dev surface
      mem   <- Vk.getPhysicalDeviceMemoryProperties dev

      pure $ Just
        ( deviceScore dev mem caps
        , PhysicalDevice
            { _pdPhysicalDevice  = dev
            , _pdProperties      = props
            , _pdGraphicsQueueIx = graphicsQueue
            , _pdPresentQueueIx  = presentQueue
            , _pdPresentMode     = presentMode
            , _pdSurfaceFormat   = bestFormat
            , _pdSurfaceCaps     = caps
            , _pdMemory          = mem
            }
        )

    deviceHasSwapChain dev = do
      (_res, extensions) <- Vk.enumerateDeviceExtensionProperties dev Nothing
      guard $
        Vector.elem
          Khr.KHR_SWAPCHAIN_EXTENSION_NAME
          (fmap Vk.extensionName extensions)

    getGraphicsQueueIndices dev = do
      queueFamilyProperties <- Vk.getPhysicalDeviceQueueFamilyProperties dev
      pure do
        (ix, q) <- zip [0..] $ toList queueFamilyProperties
        guard $
          (Vk.QUEUE_GRAPHICS_BIT .&&. Vk.queueFlags q) &&
          (Vk.queueCount q > 0)
        pure ix

    getPresentQueueIndices dev = fmap concat do
      queues <- Vk11.getPhysicalDeviceQueueFamilyProperties2 @'[] dev
      for (zip [0..] $ toList queues) \(ix, _q) -> do
        support <- Khr.getPhysicalDeviceSurfaceSupportKHR dev ix surface
        pure [ix | support]

    getFormat dev = do
      (_res, formats) <- Khr.getPhysicalDeviceSurfaceFormatsKHR dev surface
      pure case toList formats of
        [] ->
          desiredFormat
        [Khr.SurfaceFormatKHR Vk.FORMAT_UNDEFINED _colorSpace] ->
          desiredFormat
        candidates | any cond candidates ->
          desiredFormat
        whatever : _rest ->
          whatever
      where
        -- XXX: An Eq-substitute? Should be fixed in vulkan-3.4
        cond f =
          Khr.format     (f :: Khr.SurfaceFormatKHR) == Khr.format     (desiredFormat :: Khr.SurfaceFormatKHR) &&
          Khr.colorSpace (f :: Khr.SurfaceFormatKHR) == Khr.colorSpace (desiredFormat :: Khr.SurfaceFormatKHR)

    getPresentMode dev = do
      (_res, presentModes) <- Khr.getPhysicalDeviceSurfacePresentModesKHR dev surface
      pure do
        m <- desiredPresentModes
        guard $ Vector.elem m presentModes
        pure m

    desiredFormat =
      Khr.SurfaceFormatKHR
        Vk.FORMAT_B8G8R8_UNORM
        Khr.COLOR_SPACE_SRGB_NONLINEAR_KHR

    desiredPresentModes =
      [ Khr.PRESENT_MODE_MAILBOX_KHR
      , Khr.PRESENT_MODE_FIFO_KHR
      , Khr.PRESENT_MODE_IMMEDIATE_KHR
      ]

deviceScore
  :: Vk.PhysicalDevice
  -> Vk.PhysicalDeviceMemoryProperties
  -> Khr.SurfaceCapabilitiesKHR
  -> Int
deviceScore _dev mem _caps = fromIntegral totalHeapSize
  where
    totalHeapSize = sum $ do
      Vk.MemoryHeap{size} <- Vk.memoryHeaps mem
      pure size

withVulkanDevice :: Vk11.Instance -> Khr.SurfaceKHR -> PhysicalDevice -> Managed VulkanDevice
withVulkanDevice vkInstance surface pd@PhysicalDevice{..} = do
  device <- managed $ Vk.withDevice _pdPhysicalDevice deviceCI Nothing bracket

  graphicsQueue <- Vk11.getDeviceQueue2 device zero
    { Vk11.queueFamilyIndex = _pdGraphicsQueueIx
    , Vk11.flags            = Vk11.DEVICE_QUEUE_CREATE_PROTECTED_BIT
    }

  presentQueue <- Vk11.getDeviceQueue2 device zero
    { Vk11.queueFamilyIndex = _pdPresentQueueIx
    , Vk11.flags            = Vk11.DEVICE_QUEUE_CREATE_PROTECTED_BIT
    }

  allocator <- VMA.withAllocator (allocatorCI device) allocateIO

  pure VulkanDevice
    { _vdLogical   = device
    , _vdPhysical  = pd
    , _vdSurface   = surface
    , _vdGraphicsQ = graphicsQueue
    , _vdPresentQ  = presentQueue
    , _vdAllocator = allocator
    }
  where
    requiredDeviceExtensions =
      [ Khr.KHR_SWAPCHAIN_EXTENSION_NAME
      ]

    deviceCI :: Vk.DeviceCreateInfo '[]
    deviceCI = zero
      { Vk.queueCreateInfos = Vector.fromList do
          i <- nub [_pdGraphicsQueueIx, _pdPresentQueueIx]
          pure $ SomeStruct zero
            { Vk.queueFamilyIndex = i
            , Vk.queuePriorities = Vector.singleton 1
            , Vk.flags = Vk.DEVICE_QUEUE_CREATE_PROTECTED_BIT
            }
      , Vk.enabledExtensionNames =
          Vector.fromList requiredDeviceExtensions
      , Vk.enabledFeatures = Just zero
          { Vk.sampleRateShading = True
          , Vk.samplerAnisotropy = True
          }
      }

    allocatorCI device = zero
      { VMA.physicalDevice  = Vk.physicalDeviceHandle _pdPhysicalDevice
      , VMA.device          = Vk.deviceHandle device
      , VMA.instance'       = Vk.instanceHandle vkInstance
      , VMA.frameInUseCount = 1
      }
