module Import
  ( module Types
  , module RE
  , module RIO

  , (.&&.)
  ) where

-- The prelude here
import RIO

-- Base stuff
import Data.Bits as RE (Bits(..))
import Data.List as RE (sort, sortOn)
import Foreign.C.Types as RE (CInt)
import Foreign.Ptr as RE (castPtr)

-- Hackage deps
import Linear as RE (V2(..), V3(..), V4(..))
import Control.Lens as RE (use, (.=))

-- Generic Vulkan utilities
import Vulkan.CStruct.Extends as RE (SomeStruct(..))
import Vulkan.NamedType as RE ((:::))
import Vulkan.Zero as RE (zero)

-- Project stuff
import Types

(.&&.) :: Bits a => a -> a -> Bool
x .&&. y = (/= zeroBits) (x .&. y)
