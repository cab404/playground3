{-# LANGUAGE TemplateHaskell #-}

module Types where

import Control.Lens.TH (makeLenses)
import Control.Monad.Managed
import RIO
import RIO.Orphans
import RIO.Process
import Vulkan.Zero (Zero(..))

import qualified Vulkan.Core10 as Vk
import qualified Vulkan.Core11 as Vk11
import qualified Vulkan.Extensions.VK_KHR_surface as Khr
import qualified Vulkan.Extensions.VK_KHR_swapchain as Khr
import qualified VulkanMemoryAllocator as VMA

import qualified SDL

-- | Command line arguments
data Options = Options
  { optionsVerbose    :: Bool
  , optionsFullscreen :: Bool
  , optionsWindowSize :: (Int, Int)
  } deriving (Show)

data App = App
  { _appOptions        :: Options

  , _appLogFunc        :: LogFunc
  , _appProcessContext :: ProcessContext
  , _appResourceMap    :: ResourceMap

  , _appSdlWindow      :: SDL.Window
  , _appVulkanDevice   :: VulkanDevice

  , _appState          :: SomeRef AppState
  }

-- | Initialized logical device and associated resources.
data VulkanDevice = VulkanDevice
  { _vdLogical   :: Vk.Device
  , _vdPhysical  :: PhysicalDevice
  , _vdSurface   :: Khr.SurfaceKHR
  , _vdGraphicsQ :: Vk11.Queue
  , _vdPresentQ  :: Vk11.Queue
  , _vdAllocator :: VMA.Allocator
  }

-- | Physical device annotated with extracted properties.
data PhysicalDevice = PhysicalDevice
  { _pdPhysicalDevice  :: Vk.PhysicalDevice
  , _pdProperties      :: Vk.PhysicalDeviceProperties
  , _pdGraphicsQueueIx :: Word32
  , _pdPresentQueueIx  :: Word32
  , _pdPresentMode     :: Khr.PresentModeKHR
  , _pdSurfaceFormat   :: Khr.SurfaceFormatKHR
  , _pdSurfaceCaps     :: Khr.SurfaceCapabilitiesKHR
  , _pdMemory          :: Vk.PhysicalDeviceMemoryProperties
  }

data AppState = AppState
  { _asQuitApp     :: Bool
  , _asQuitContext :: Bool
  }

instance Zero AppState where
  zero = AppState
    { _asQuitApp     = False
    , _asQuitContext = False
    }

-- | Vulkan bits that change through the window lifetime (on resize, context change, etc.)
data VulkanContext = VulkanContext
  { _vcSwapChain   :: Khr.SwapchainKHR
  , _vcCommandPool :: Vk.CommandPool
  , _vcSwapViews   :: Vector Vk.ImageView
  , _vcExtent      :: Vk.Extent2D
  }

makeLenses ''App
makeLenses ''VulkanDevice
makeLenses ''PhysicalDevice
makeLenses ''AppState
makeLenses ''VulkanContext

instance HasLogFunc App where
  logFuncL = appLogFunc

instance HasProcessContext App where
  processContextL = appProcessContext

instance HasResourceMap App where
  resourceMapL = appResourceMap

allocateIO :: IO a -> (a -> IO b) -> Managed a
allocateIO c d = managed (bracket c d)

instance HasStateRef AppState App where
  stateRefL = appState
