{-# LANGUAGE TemplateHaskell #-}

module Main (main) where

import Import

import Options.Applicative.Simple
import Control.Monad.Managed (runManaged)

import Run (run)
import App.Setup (setup)

import qualified Paths_playground3

main :: IO ()
main = do
  (options, ()) <- simpleOptions
    $(simpleVersion Paths_playground3.version)
    "Header for command line arguments"
    "Program description, also for command line arguments"
    parseOptions
    empty

  runManaged do
    app <- setup options
    liftIO $ runRIO app run

parseOptions :: Parser Options
parseOptions = do
  optionsVerbose <- switch $ mconcat
    [ long "verbose"
    ]

  optionsFullscreen <- switch $ mconcat
    [ long "fullscreen"
    ]

  optionsWindowSize <- option disabled $ mconcat
    [ long "window-size"
    , value (800, 600)
    ]

  pure Options{..}
